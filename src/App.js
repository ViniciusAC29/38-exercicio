import { Component } from "react";
import PeopleList from "./components/peopleList/index";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      people: [],
      url: "http://hp-api.herokuapp.com/api/characters/students",
    };
  }

  getPeople = (url) => {
    const { people } = this.state;
    fetch(url)
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
        console.log(response.length);
        return this.setState({ people: [...people, ...response] });
      });
  };

  componentDidMount() {
    this.getPeople(this.state.url);
  }

  render() {
    const peopleList = this.state.people;
    return (
      <div className="App">
        <PeopleList className="App-header" list={peopleList} />
      </div>
    );
  }
}

export default App;
