import { Component } from "react";
import Wizard from "../wizard";

class PeopleList extends Component {
  render() {
    const newWizardList = () => {
      const wizardListProp = this.props.list;
      if (wizardListProp.length === 0) {
        return [];
      }

      let arrResult = [];

      console.log(wizardListProp);

      for (let i = 0; i < 3; i++) {
        let number = Math.floor(Math.random() * wizardListProp.length);
        console.log(number);
        console.log(wizardListProp[number]);
        arrResult.push(wizardListProp[number]);
      }

      console.log(arrResult);
      return arrResult;
    };

    return (
      <div>
        {newWizardList().map((persona, i) => (
          <Wizard
            key={i}
            name={persona.name}
            figureFace={persona.image}
            house={persona.house}
          />
        ))}
      </div>
    );
  }
}
export default PeopleList;
