import { Component } from "react";

class Wizard extends Component {
  render() {
    const { name, figureFace, house } = this.props;

    return (
      <div>
        <img src={figureFace}></img>
        <h3>{name}</h3>
        {house}
      </div>
    );
  }
}
export default Wizard;
